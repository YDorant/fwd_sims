#!/bin/bash

module load vcftools

#get global variables
model=$1
NCPU=$2

#check global variable parsing
if [ -z "$model" ]
then
    echo "error! please provide model name"
    exit    
fi

if [ -z "$NCPU" ]
then
    NCPU=1
fi


#create folders to recieve each maf filtering dataset
for maf in $(cat list_maf.txt) ;
    do
    #echo "$maf"
    mkdir -p 02_vcf/"$model"/"$model"_maf"$maf"
    mkdir -p 02_vcf/"$model"/"$model"_maf"$maf"/00-vcffst
    done


#prepare StAMPP files filtered by maf value
for maf in $(cat list_maf.txt) ;
    do
    echo "$maf"
    for vcf in $(ls 02_vcf/"$model"/slim.*.vcf);
        do
        echo $vcf
        NameVcf=$(basename $vcf)
        #echo "$NameVcf"
        bash Toolbox/00-VCF_Reshaper.sh -v $vcf \
                            -p 01_info_file/strata.txt \
                            -f StAMPP \
                            -m $maf \
                            -o 02_vcf/"$model"/"$model"_maf"$maf"/"$NameVcf"_maf"$maf"
        rm 02_vcf/"$model"/"$model"_maf"$maf"/*.012*
        done
    done

#extract number of SNPs for each maf threshold
if [ -e "02_vcf/$model"/SNP_counts.txt ]; then
	rm SNP_counts.txt
fi

echo "model maf rep N_SNPs" > "02_vcf/$model"/SNP_counts.txt
for maf in $(cat list_maf.txt);
    do
    for file in $(ls 02_vcf/"$model"/"$model"_maf"$maf"/slim*.StAMPP);
        do
        REP=$(basename $file | cut -f 2 -d'.')
        SITES=$(cut -f 5- "$file" | awk '{print NF}' | head -n1)
        echo "$model $maf $REP $SITES" >> "02_vcf/$model"/SNP_counts.txt
        done
    done


#Define func loop for FST calculation
fst_func(){
        echo $1
        FileName=$(basename $1 | sed 's/.StAMPP//g')
        OutputDir=$(dirname $1)
        Rscript Toolbox/StAMPP-fst.R $1 "$OutputDir"/00-vcffst/"$FileName"
}

export -f fst_func

# perform fst calculation over each maf folder with parallel
for maf in $(cat list_maf.txt) ;
    do
    parallel -j "$NCPU" fst_func ::: 02_vcf/"$model"/"$model"_maf"$maf"/*.StAMPP
    done


#resume all Fst results in a datafile
Rscript 00_scripts/rscript/02.resume_fst_model.R list_maf.txt $model 

mv 02_vcf/"$model"/mean_fst_model.*.txt 03_results/


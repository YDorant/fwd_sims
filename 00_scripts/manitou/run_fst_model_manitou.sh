#!/bin/bash
#SBATCH -J "Fst_modB"
#SBATCH -o log_%j
#SBATCH -c 1
#SBATCH -p small
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=YOUREMAIL
#SBATCH --time=24:00:00
#SBATCH --mem=2G

cd $SLURM_SUBMIT_DIR             

model=$1 #basename of the model example: model.A. 

if [ -z "$model" ]
then
    echo "Error: need model name (eg: model.A)"
    echo "this corresponds to the name of the slim model we want to run"
    echo "models are stored in folder `00_scripts/models/` "
    exit
fi

./00_scripts/02.slim_fst.general.sh $model

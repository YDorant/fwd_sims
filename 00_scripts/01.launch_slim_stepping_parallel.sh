#!/bin/bash

ITERATION=$1
model=$2 #"model.A"
NCPU=$3

if [ -z "$ITERATION" ]
then
    echo "error please provide number of ITER"
    exit    
fi

#model=$2
if [ -z "$model" ]
then
    echo "error! please provide model name"
    exit    
fi

if [ -z "$NCPU" ]
then
    ! No NCPU value parsed ---> used CPU=1
    NCPU=1
fi

mkdir -p 02_vcf/$model

# launch slim file
for i in $(eval echo "{1..$ITERATION}")
do
  toEval="cat 00_scripts/models/script_slim_template."$model".sh | \
      sed 's/__NB__/$i/g' | \
      sed 's/__mode__/$model/g' "
      eval $toEval > SLIM_"$model"_"$i".sh
done


parallel -j"$NCPU" slim ::: SLIM_"$model"_*.sh

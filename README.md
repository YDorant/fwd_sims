# fwd_sims

## Pipeline for Forward simulations [SLiM]

#### Contributors: Yann Dorant & Quentin Rougemont

#### Correspondence <yann.dorant.1@ulaval.ca>

#### license: Free

## Overview

Simple pipeline to perform forward simulation with SLiM and perform Fst calculations

The pipeline is implemented with 1D stepping-stone models (4 populations)

## Dependencies:

* SLiM [Forward simulator](https://messerlab.org/slim/)
* vcftools [vcftools.org](http://vcftools.sourceforge.net/index.html)
* R
* YDorant/Toolbox --> git clone https://gitlab.com/YDorant/Toolbox

## Running a model:

```./00_scripts/01.launch_slim_stepping.sh 10 model.A```

```./00_script/02.slim.fst_general.sh model.A```